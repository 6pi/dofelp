import pandas as pd
from datetime import datetime

def parseCSV(file):
	df = pd.read_csv(file, sep=",")
	#CHECK LE HEADER
	if not (set(['nom','categorie','prix']).issubset(df.columns)):
		return redirect(url_for("index", error="CSV pas compatible"))
	conn = psycopg2.connect(host='localhost', dbname='client')
	print('Connected to the database')
	cur = conn.cursor()
	try:
		date = datetime(file.replace(".csv", ""))
	except Exception as e:
		return redirect(url_for("index", error=e))
	for i in df:
		#CREATION DE LA DB
		try:
			#check existance DB
			command = "CREATE TABLE IF NOT EXISTS " + str(i["nom"]) + \
				"date DATE," + \
				"prix Integer"
			cur.execute(command)
		except Exception as e:
			return redirect(url_for("index", error=e))
		#AJOUT DES DONNEES DATE ET PRIX
		try:
			command = "INSERT INTO " + str(i["nom"]) + "(date, prix)" + \
				"VALUES (" + date + "," + str(i["prix"]) + ")"
			cur.execute(command)
		except Exception as e:
			return redirect(url_for("index", error=e))
		# AJOUT DE L ITEM A LA GROSSE BD
		try:
			command = "INSERT INTO dofus (nom, categorie, niveau) VALUES (" \
				+ str(i["nom"]) + ", " + str(i["categorie"]) + ", " + str(i["niveau"]) + ")"
			cur.execute(command)
		except Exception as e:
			return redirect(url_for("index", error=e))
