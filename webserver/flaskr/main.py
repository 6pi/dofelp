import os
from flask import *
import psycopg2
from querys import *
from csvparser import *

# create and configure the app
# Upload folder

app = Flask(__name__, instance_relative_config=True)
app.config.from_mapping(
	SECRET_KEY='dev',
	DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
)

UPLOAD_FOLDER = 'static/files'
app.config['UPLOAD_FOLDER'] =  UPLOAD_FOLDER


# ensure the instance folder exists
try:
	os.makedirs(app.instance_path)
except OSError:
	pass

# a simple page that says hello
@app.route('/hello')
def hello():
	return 'Hello, World!'

@app.route('/')
@app.route('/<error>')
def index(error = None):
	return render_template("upload.html", hasError = error)

@app.route('/items')
def items():
	return render_template("items.html", nom=get_nom())

# Get the uploaded files
@app.route("/", methods=['POST'])
def uploadFiles():
      # get the uploaded file
      uploaded_file = request.files['file']
      if uploaded_file.filename != '':
           file_path = os.path.join(app.config['UPLOAD_FOLDER'], uploaded_file.filename)
          # set the file path
           uploaded_file.save(file_path)
           parseCSV(file_path)
          # save the file
      return redirect(url_for('index'))


if __name__ == "__main__":
	app.run(debug=True)
