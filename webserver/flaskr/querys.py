import psycopg2

from flask import *

def connect():
	return psycopg2.connect(host="localhost",database="postgres",user="postgres",password="root")

def get_nom():
	try:
		conn = connect()
		cur = conn.cursor()
		command = 'SELECT nom FROM dofus'
		try:
			cur.execute(command)
			rows = cur.fetchall()
			cur.close()
			conn.close()
			return rows
		except Exception as e :
			return redirect(url_for('index', error = str(e)))
	except Exception as e:
		return redirect(url_for('index', error = str(e)))
