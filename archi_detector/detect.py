import pyautogui
from playsound import playsound
import time
import subprocess
import telegram_send
import pytesseract
import cv2
import pygame
from threading import Thread
from tkinter import *
from Butons import *
import os
from PIL import Image

def locateArchis(params):
	time_archi = 0
	while (params["active"]):
		image = pyautogui.locateOnScreen(os.path.join('media' ,'archi.png'), region=params["region"][params["ecran"]-1], confidence=0.70)
		if (image != None and params["active"] and time.time() >= time_archi + 2):
			#name = 'Screearchis' + str(int(time.time())) + '.png'
			##fp = open(name, 'x')
			#fp.close()
			im = pyautogui.screenshot(os.path.join('Screens','archis' + str(int(time.time())) + '.png'), region=params["region"][params["ecran"]-1])
			if (params["auto_click"]):
				pt = pyautogui.center(image)
				y = pt.y + 40
				pyautogui.moveTo(pt.x, y, tween=pyautogui.easeInOutQuad)
				pyautogui.click()
			#take a screenshot of the position region
			im = pyautogui.screenshot("pos.png", region=params["pos"][params["ecran"]-1])
			img = cv2.imread("pos.png")
			#get the text from the position region
			text = pytesseract.image_to_string(img)
			print("ARCHI ! en " + text)
			#send an alert on telegram
			#to configure check the 'telegram-send --configure' command
			if (params["telegram"]):
				telegram_send.send(messages=["archi : " + text])
			if (params["sound"]):
				playsound(son)
			time_archi = time.time()
		time.sleep(.15)
	return

#add_switch color
def click_telegram(params):
	params["telegram"] = 0 if params["telegram"] else 1

def click_active(params):
	params["active"] = 0 if params["active"] else 1
	if (params["active"] == 1):
		archis_thread = Thread(target=locateArchis, args=(params, ))
		archis_thread.start()


def click_sound(params):
	params["sound"] = 0 if params["sound"] else 1

def click_auto_click(params):
	params["auto_click"] = 0 if params["auto_click"] else 1

#add switch ecran
def click_ecran(params):
	params["ecran"] = params["ecran"]+1 if params["ecran"] != params["nb ecran"] else 1

#different region of the screen to look at
#region : the region of the game on your screen
#pos : the region where the map position is displayed

def display_status(params):
	for x, y in params.items():
		print(x + " : " + str(y))

def get_screen_info(ecran):
	if (ecran == 1):
		region = (633, 23, 1300, 910)
		pos = (299,70, 92, 31)
	elif (ecran == 2):
		region = (2908, 26, 1230, 910)
		pos = (2573, 70, 92, 31)
	return region, pos

from sys import platform
if platform == "darwin":
	print('MAC')
elif platform == "win32":
	pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

#seting up a var for the first archi found
res = (250, 250)
params = {
	"auto_click": 1,
	"telegram": 1,
	"ecran":1,
	"nb ecran": 2,
	"active": 1,
	"sound" : 1,
	"region": [(633, 23, 1300, 910), (2908, 26, 1230, 910)],
	"pos": [(299,70, 92, 31), (2573, 70, 92, 31)]
}


pygame.init()
screen = pygame.display.set_mode(res)
pygame.display.set_caption('Dofelp')
pygame.font.init()

ecran_button = Buton_screen(0, 0, res[0]/2,  res[1]/2, (255,255,255), params["ecran"], click_ecran, params["nb ecran"],"screen.png")
sound_button = Buton(0, res[1]/2, res[0]/2, res[1]/2, (255, 0, 0), (0,255,0), click_sound, params["sound"],"sound.png")
telegram_button = Buton(res[0]/2 , 0, res[0]/2, int(res[1]/3), (255, 0, 0), (0,255,0), click_telegram, params["telegram"],"telegram.png")
click_button = Buton(res[0]/2, int(res[1]/3), res[0]/2, int(res[1]/3), (255, 0, 0), (0,255,0), click_auto_click, params["auto_click"], "auto.png")
activate_button = Buton(res[0]/2, 2 * int(res[1]/3), res[0]/2, int(res[1]/3), (255, 0, 0), (0,255,0), click_active,params["active"], "start.png")
buttons = [ecran_button, sound_button, telegram_button, click_button, activate_button]

display_status(params)

#setting up path to the sound
son = subprocess.check_output("echo %cd%", shell=True, universal_newlines=True)
son = son[:-1]
son = os.path.join(os.path.join(son, 'media'), 'archi.mp3')
print(son)
archis_thread = Thread(target=locateArchis, args=(params, ))
archis_thread.start()
process = 1
for bu in buttons:
	bu.draw(screen)
pygame.display.update()
while (process):
	for event in pygame.event.get():
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_s:
				display_status(params)
		if event.type == pygame.QUIT:
			pygame.quit()
			params["active"] = 0
			process = 0
			for bu in buttons:
				del bu.image
				del bu.rect
			exit()
		if event.type == pygame.MOUSEBUTTONDOWN:
			mouse = pygame.mouse.get_pos()
			for bu in buttons:
				if (bu.is_over(mouse)):
					bu.action(params)
					bu.draw(screen)
					pygame.display.update()
	#search on screen for an image looking like 'archi.png' in the region
	time.sleep(.15)
