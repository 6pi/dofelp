import cv2
import math
import os
import json
import time
import pytesseract
from PIL import Image
import PIL
import pandas as pd
import numpy as np
from threading import Thread


def extractor(proportions, width, height, objects):
	df = pd.DataFrame(columns=["Nom", "Catégorie", "Niveau", "Prix moyen"])
	files = os.listdir("scrapscreens")
	print(proportions)

	items = []
	pady = height / objects
	pourcent = 0
	count = int(1. / len(files) *100)
	for file in files:
		print(str(pourcent) + "%")
		img = cv2.imread(os.path.join("scrapscreens", file))
		for y in range(objects):
			txt = []
			for x in range(len(proportions) -2):
				temp_read = img[int(y * pady):int((y+1) * pady),int(proportions[x] * width):int(proportions[x+1] * width),:]
				temp_read = cv2.cv2.cvtColor(temp_read, cv2.COLOR_BGR2GRAY)  # convert to grey
				if (x > 2):
					txt.append(pytesseract.image_to_string(temp_read, config='-c tessedit_char_whitelist=0123456789').replace("\n", "").replace("\"", ""))
				else:
					txt.append(pytesseract.image_to_string(temp_read).replace("\n",  "").replace("\"", ""))
				#if (txt[-1] == "" or txt[-1] == " "):
					#cv2.imshow("error", temp_read)
					#cv2.waitKey(0)
			df = df.append(dict(zip(df.columns,txt)), ignore_index=True)
		pourcent += count
		os.remove(os.path.join("scrapscreens", file))
	df = df.drop_duplicates(subset=["Nom"])
	df.to_csv(str(time.time()) + ".csv", index=False)
	print("Done")
