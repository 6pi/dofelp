#Importing the tkinter library
from tkinter import *
from tkinter import ttk
import pytesseract
import pyautogui
import os
import time
from extractor import *

#number of objects display on one page
objects = 14
proportions = [0.62/8, 3.5/8, 5.3/8, 6.2/8, 7.57/8]
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
stop = 0

def stop_scrap():
	stop = 1

def reset_scrap():
	stop = 0

def create_grid(event=None):
	canvas.delete("grid_line")
	height = grid.winfo_height()
	width = grid.winfo_width()
	pady = height / objects
	for i in range(0, objects):
		canvas.create_line([(0, pady*i) , (width, pady*i)], width=2, fill="black", tag="grid_line")
	for i in proportions:
		canvas.create_line([(int(i * width), 0) , (int(i * width), height)], width=2, fill="black", tag="grid_line")




def show_grid():
	if (var_grid.get() == 0):
		grid.withdraw()
	else:
		grid.deiconify()

def scrap():
	grid.withdraw()
	x, y = grid.winfo_x(), grid.winfo_y()
	print(x, y)
	width, height = grid.winfo_width(), grid.winfo_height()
	print(width, height)
	#height of the window top bar
	bar = 30
	pyautogui.moveTo(x + 50, y + 50)
	pyautogui.click()
	time.sleep(1)
	pyautogui.click()
	pyautogui.moveTo(x - 50, y - 50)
	i = 0
	c = True
	while (c):
		image = pyautogui.screenshot(os.path.join('scrapscreens', str(i) + '.png'), region=[x, y+bar, width, height])
		i+=1
		image = pyautogui.locateOnScreen('stop.png', region=(1530,840, 20, 20))
		if (image != None):
			print("stop")
			c = 0
		for j in range(14):
			pyautogui.press('down')
		time.sleep(1)

	proportions.append(1)
	print("go")
	t = time.time()
	extractor(proportions, width, height ,objects)
	print(str(time.time() - t))


win = Tk()
win.title("HDV Scraper")
win.attributes('-topmost', True)
win.geometry("200x200")


valid = Button(win, text="Scrap", command=scrap)
valid.grid(column=0, row=1)

var_grid = IntVar()
show = Checkbutton(win, text="show grid", variable=var_grid, onvalue=1, offvalue=0, command=show_grid)
show.grid(column=0, row=0)

help = Button(win, text="Stop", command=stop_scrap)
help.grid(column=1, row = 0)

grid = Toplevel(win)
grid.geometry("595x652+928+173")
grid.attributes('-alpha', 0.3)
grid.attributes('-topmost', True)

canvas = Canvas(grid,  bg='white')
canvas.pack(fill=BOTH, expand=True)
canvas.bind('<Configure>', create_grid)

grid.withdraw()
win.mainloop()
