import pyshark
import socket
import netifaces

def get_public_ip():
	interfaces = netifaces.interfaces()
	for i in interfaces:
		if i == 'lo':
			continue
		iface = netifaces.ifaddresses(i).get(netifaces.AF_INET)
		if iface != None:
			for j in iface:
				if (j['addr'][:3] != "127"):
					return j['addr']


capture = pyshark.LiveCapture(interface='en0', bpf_filter='tcp port 5555 and len > 66')

def get_id_from_packet(data):
	packet_id = data[:4]
	print(packet_id)
	packet_id = bin(int(data, 16))
	print(packet_id)
	packet_id = packet_id[:-2]
	print(packet_id)
	packet_id = int(packet_id, 2)
	print(packet_id)
	return packet_id

my_ip = get_public_ip()
for packet in capture.sniff_continuously(packet_count=20):
	if packet.ip.src == my_ip:
		print("[DEBUG] Paquet Dofus Envoyé")
		try:
			print(packet)
			print(packet.data)
			packet = packet.data.data
			id = get_id_from_packet(packet)
			print(id)
			print(packet)
		except (AttributeError):
			print("CACA")
	else:
		print("[DEBUG] Paquet Dofus Reçu")
