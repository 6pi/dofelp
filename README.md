# Dofelp

## Description
Group of tools to make life easier for dofus players.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Python 3.6+ needed.
``` 
pip install -r requierement.txt
```

## Usage
### Archimonster Detector
To activate automatic telegram message, set up Telegram-sent with :
```
telegram-send configure
```
To launch the archi monster detector:
```
python detect.py
```
Dual screen is not support yet, it needs an edit of the PyAutoGUI lib, do it at your own risks.

## Roadmap
-reliquat calculator using a sniffer
-HDV's prices collector to build DB using only image recognition
