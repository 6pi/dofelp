import pyautogui
from playsound import playsound
import time
import subprocess
from  pynput.keyboard import Listener   #importing Listener for Keyboard activity
import logging  #importing logging module for creating log file of keyboard activity
import pyautogui
from pynput import *
import random

ecran = 1
ok = 1

if (ecran == 1):
	region = (633, 23, 1300, 910)
	gauche = [57, 716, 268, 290]
	droite = [57, 716, 1885, 2291]
	haut = [25, 39, 269, 1884]
	bas = [913, 934, 269, 1884]
elif (ecran == 2):
	region = (2908, 26, 1230, 910)
	gauche = []
	droite = []
	haut = []
	bas = []

def click_zone(zone):
	return (random.randint(zone[2], zone[3]), random.randint(zone[0], zone[1]))

son = subprocess.check_output("echo %cd%", shell=True, universal_newlines=True)
son = son[:-1]
son += '\\archi.mp3'

file_path="key_log.txt" #mention the log file destination path here

def on_press(key):  #Function for key press action
	global ok
	logging.info("Key pressed : {0}".format(key))
	print(str(key))
	if (str(key)[1] == 'q'):
		ok = 0 if ok == 1 else 1
	"""if (ok):
		if (str(key) == 'Key.left'):
			x, y = click_zone(gauche)
			pyautogui.moveTo(x, y, duration=1, tween=pyautogui.easeInOutQuad)
			pyautogui.click()
		elif (str(key) == 'Key.right'):
			x, y = click_zone(droite)
			pyautogui.moveTo(x, y, duration=1, tween=pyautogui.easeInOutQuad)
			pyautogui.click()
		elif (str(key) == 'Key.down'):
			x, y = click_zone(bas)
			pyautogui.moveTo(x, y, duration=1, tween=pyautogui.easeInOutQuad)
			pyautogui.click()
		elif (str(key) == 'Key.up'):
			x, y = click_zone(haut)
			pyautogui.moveTo(x, y, duration=1, tween=pyautogui.easeInOutQuad)
			pyautogui.click()"""
	if (str(key)[1] == 'b'):
		print(pyautogui.position())



def on_release(key):	#function for key release action
	logging.info("Key released: {0}".format(key))

#Driver code
logging.basicConfig(filename=file_path, level=logging.DEBUG, format='%(asctime)s: %(message)s')  #Creating log file in txt form
with Listener(on_press=on_press, on_release=on_release) as l:   #whenever key is pressed or released
	l.join()	#both the process functions are executed
