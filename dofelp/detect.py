import pyautogui
from playsound import playsound
import time
import subprocess
import telegram_send
import pytesseract
import cv2
import pygame
from threading import Thread
from tkinter import *
from Butons import *
import os
from PIL import Image


def locateArchis(params):
	time_archi = 0
	while (params["active"]):
		image = pyautogui.locateOnScreen(os.path.join('media' ,'archi.png'), region=params["region"][params["ecran"]-1], confidence=0.70)
		if (image != None and params["active"] and time.time() >= time_archi + 2):
			#name = 'Screearchis' + str(int(time.time())) + '.png'
			##fp = open(name, 'x')
			#fp.close()
			params["autopilote_pause"] = 1
			im = pyautogui.screenshot(os.path.join('Screens','archis' + str(int(time.time())) + '.png'), region=params["region"][params["ecran"]-1])
			if (params["auto_click"]):
				pt = pyautogui.center(image)
				y = pt.y + 40
				pyautogui.moveTo(pt.x, y, tween=pyautogui.easeInOutQuad)
				pyautogui.click()
			#take a screenshot of the position region
			im = pyautogui.screenshot("pos.png", region=params["pos"][params["ecran"]-1])
			img = cv2.imread("pos.png")
			#get the text from the position region
			text = pytesseract.image_to_string(img)
			print("ARCHI ! en " + text)
			#send an alert on telegram
			#to configure check the 'telegram-send --configure' command
			if (params["telegram"]):
				telegram_send.send(messages=["archi : " + text])
			if (params["sound"]):
				playsound(params["son"])
			time_archi = time.time()
		time.sleep(.15)
	return
