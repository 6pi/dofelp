from playsound import playsound
import time
import subprocess
import telegram_send
import pytesseract
import cv2
import pygame
from threading import Thread
from Butons import *
import os
from PIL import Image
from sys import platform
from detect import *
from autopilote import *

#add_switch color
def click_telegram(params):
	params["telegram"] = 0 if params["telegram"] else 1

def click_active(params):
	params["active"] = 0 if params["active"] else 1
	if (params["active"] == 1):
		archis_thread = Thread(target=locateArchis, args=(params, ))
		archis_thread.start()

def click_sound(params):
	params["sound"] = 0 if params["sound"] else 1

def click_auto_click(params):
	params["auto_click"] = 0 if params["auto_click"] else 1

#add switch ecran
def click_ecran(params):
	params["ecran"] = params["ecran"]+1 if params["ecran"] != params["nb ecran"] else 1

def click_autopilote(params, file):
	params["autopilote"] = 0 if params["autopilote"] else 1
	if (params["autopilote"] == 1):
		print("active Thread on " + file)
		autopilote_thread = Thread(target=autopilote, args=(params, file, ))
		autopilote_thread.start()

def click_pause(params):
	params["autopilote_pause"] = 0 if params["autopilote_pause"] else 1

def click_stop(params):
	params["autopilote"] = 0


def display_status(params):
	for x, y in params.items():
		print(x + " : " + str(y))

if __name__ == "__main__":
	if platform == "darwin":
		print('MAC')
	elif platform == "win32":
		pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'


	#seting up a var for the first archi found
	res = (250, 700)
	params = {
		"auto_click": 1,
		"telegram": 1,
		"ecran":1,
		"nb ecran": 2,
		"active": 1,
		"sound" : 1,
		"region": [(633, 23, 1300, 910), (2908, 26, 1230, 910)],
		"pos": [(299,70, 92, 31), (2573, 70, 92, 31)],
		"autopilote" : 0,
		"autopilote_pause" : 0,
		"cmd": [(574, 1037), ()],
		"gauche" : [239, 590, 633, 664],
		"droite" : [219, 553, 1896, 1942],
		"haut" : [25, 38, 986, 1541],
		"bas": [914, 929,1160, 1698]
	}


	pygame.init()
	screen = pygame.display.set_mode(res)
	pygame.display.set_caption('Dofelp')
	pygame.font.init()

	ecran_button = Buton_screen(0, 0, res[0]/2,  res[1]/4, (255,255,255), params["ecran"], click_ecran, params["nb ecran"],"screen.png")
	sound_button = Buton(0, res[1]/4, res[0]/2, res[1]/4, (255, 0, 0), (0,255,0), click_sound, params["sound"],"sound.png")
	telegram_button = Buton(res[0]/2 , 0, res[0]/2, int(res[1]/6), (255, 0, 0), (0,255,0), click_telegram, params["telegram"],"telegram.png")
	click_button = Buton(res[0]/2, int(res[1]/6), res[0]/2, int(res[1]/6), (255, 0, 0), (0,255,0), click_auto_click, params["auto_click"], "auto.png")
	activate_button = Buton(res[0]/2, 2 * int(res[1]/6), res[0]/2, int(res[1]/6), (255, 0, 0), (0,255,0), click_active,params["active"], "start.png")
	pause_button = Buton(res[0]/2, res[1]/2, res[0]/2, int(res[1]/4), (255, 0, 0), (0,255,0), click_pause, 0, "pause.png")
	stop_button = Buton(res[0]/2, res[1]/2+res[1]/4, res[0]/2, int(res[1]/4), (255, 255, 255), (255,255,255), click_stop, 0, "stop.png")
	apfile = os.listdir("path")
	apfile_buttons = []
	for i in range(0, len(apfile)):
		apfile_buttons.append(Buton_text(0, res[1]/2 + i * int(res[1]/len(apfile)/2), res[0]/2, int(res[1]/len(apfile)/2), (255, 0, 0), (0,0,255) , click_autopilote, apfile[i].replace(".txt", ""),  os.path.join("path", apfile[i])))
	buttons = [ecran_button, sound_button, telegram_button, click_button, activate_button, pause_button, stop_button]

	display_status(params)
	son = subprocess.check_output("echo %cd%", shell=True, universal_newlines=True)
	son = son[:-1]
	son = os.path.join(os.path.join(son, 'media'), 'archi.mp3')
	params["son"] = son
	#setting up path to the sound

	archis_thread = Thread(target=locateArchis, args=(params, ))
	archis_thread.start()
	autopilote_thread = None
	process = 1
	for bu in buttons:
		bu.draw(screen)
	for bu in apfile_buttons:
		bu.draw(screen)
	pygame.display.update()
	while (process):
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_s:
					display_status(params)
				if event.key == pygame.K_a:
					params["autopilote"] = 1
					autopilote_thread = Thread(target=autopilote, args=(os.path.join("path", "corbac.txt"), params, ))
					autopilote_thread.start()
				if event.key == pygame.K_s:
					params["autopilote"] = 0

			if event.type == pygame.QUIT:
				pygame.quit()
				params["active"] = 0
				params["autopilote"] = 0
				process = 0
				for bu in buttons:
					del bu.image
					del bu.rect
				exit()
			if event.type == pygame.MOUSEBUTTONDOWN:
				mouse = pygame.mouse.get_pos()
				for bu in buttons:
					if (bu.is_over(mouse)):
						bu.action(params)
						bu.draw(screen)
						pygame.display.update()
				for bu in apfile_buttons:
					if (bu.is_over(mouse)):
						if (params["autopilote"] == 0 or (params["autopilote"] and params["file"] == bu.file)):
							params["file"] = bu.file
							bu.action(params, bu.file)
							bu.draw(screen)
							pygame.display.update()

		#search on screen for an image looking like 'archi.png' in the region
		time.sleep(.15)
