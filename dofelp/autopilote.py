import pyautogui
from playsound import playsound
import time
import subprocess
import telegram_send
import pytesseract
import cv2
import pygame
from threading import Thread
from tkinter import *
from Butons import *
import os
from PIL import Image
import sys
import random


#region : the region of the game on your screen
#pos : the region where the map position is displayed

def click_zone(zone):
	x, y = (random.randint(zone[2], zone[3]), random.randint(zone[0], zone[1]))
	pyautogui.moveTo(x, y, duration=1, tween=pyautogui.easeInOutQuad)
	pyautogui.click()
	time.sleep(2)

def press_string(str):
	for i in str:
		pyautogui.press(i)

def press_enter():
	pyautogui.press('enter')

def get_pos(params):
	im = pyautogui.screenshot("pos.png", region=params["pos"][params["ecran"]-1])
	img = cv2.imread("pos.png")
	actual_pos = pytesseract.image_to_string(img)
	pos = ""
	n = 0
	actual_pos = actual_pos.replace(')', ",")
	actual_pos = actual_pos.replace('=', "-")
	#a ameliorer
	for i in actual_pos:
		if (i == '-'):
			pos += i
		if ('0' < i <= '9'):
			pos += i
		if(i == ',' and n == 0):
			pos += ' '
			n+=10
	pos += "\n"
	return pos

#fielname = sys.argv[1]
def autopilote(params, filename):
	print("Open file : " + filename)
	with open(filename) as fd:
		pos = None
		while (params["autopilote"] == 1):
			while (params["autopilote_pause"] == 1):
				time.sleep(2.5)
			line = fd.readline()
			if not line:
				break
			if (line[0].isalpha()):
				print("line")
				click_zone(params[line.strip("\n")])
			else:
				pyautogui.moveTo(params["cmd"][params["ecran"]-1][0], params["cmd"][params["ecran"]-1][1], tween=pyautogui.easeInOutQuad)
				pyautogui.click()
				press_string("/travel " + line)
				pos = line
				new_pos = ""
				press_enter()
				time.sleep(1.5)
				press_enter()
				print("target pos : " + str(pos))
				old_pos = new_pos
				i = 0
				while (new_pos != pos and i < 3 and params["autopilote"] == 1):
					while (params["autopilote_pause"] == 1):
						time.sleep(2.5)
					old_pos = new_pos
					new_pos = get_pos(params)
					#avoid getting stuck where position can not be read
					if ((old_pos != "" or old_pos != "\n") and old_pos == new_pos):
						i+= 1
					else:
						i = 0
					if (i > 5):
						break
					print("|" + new_pos + "|")
					time.sleep(2.5)
			time.sleep(2.5)
	print("autopilote OFF")
	return
