import pygame
import os

class Buton():
	def __init__(self, left, top, width, height, color_false, color_true, f, active, image=None):
		self.left = left
		self.top = top
		self.width = width
		self.height = height
		self.color = (color_false, color_true)
		self.action = f
		self.active = active
		self.rect = pygame.Rect(self.left, self.top, self.width, self.height)
		if (image):
			self.image = pygame.image.load(os.path.join('Sprites', image)).convert_alpha()
			self.image = pygame.transform.scale(self.image, (self.width, self.height))
		else:
			self.image = None
		self.tmp = None
		self.tmp_image = None
	def is_over(self, pos):
		if (self.left < pos[0] < self.left + self.width and self.top < pos[1] < self.top + self.height):
			self.active = 1 if self.active == 0 else 0
			return True
		return False
	def draw(self, screen):
		if (self.tmp):
			del self.tmp
		self.tmp = pygame.draw.rect(screen,self.color[self.active],self.rect)
		if (self.image):
			if (self.tmp_image):
				del self.tmp_image
			self.tmp_image = screen.blit(self.image, (self.left, self.top))


class Buton_screen(Buton):
	def __init__(self, left, top, width, height, color, ecran, f, nb_ecran,image=None):
		super().__init__(left, top, width, height, color, color, f, 1, image)
		self.color = (color,color)
		self.ecran = ecran
		self.nb_ecran = nb_ecran
		self.font = pygame.font.SysFont('Comic Sans MS', 30)
		self.text = self.font.render(str(self.ecran), False, (0,0,0))
		self.tmp_text = None

	def is_over(self, pos):
		if (self.left < pos[0] < self.left + self.width and self.top < pos[1] < self.top + self.height):
			self.ecran = self.ecran+1 if self.ecran != self.nb_ecran else 1
			self.text = self.font.render(str(self.ecran), False, (0,0,0))
			return True
		return False

	def draw(self, screen):
		super().draw(screen)
		if (self.tmp_text):
			del self.tmp_text
		self.tmp_text = screen.blit(self.text, ((self.left + self.width) / 2 - self.text.get_width()/2, (self.top + self.height)/2 - self.text.get_height()/2))

class Buton_text(Buton):
		def __init__(self, left, top, width, height, color_false, color_true, f, text, file):
			super().__init__(left, top, width, height, color_false, color_true, f, 0)
			self.font = pygame.font.SysFont('Comic Sans MS', 12)
			self.text = self.font.render(text, False, (0,0,0))
			self.file = file
			self.tmp_text = None

		def draw(self, screen):
			super().draw(screen)
			if (self.tmp_text):
				del self.tmp_text
			self.tmp_text = screen.blit(self.text, (self.left, self.top))
